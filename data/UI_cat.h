#ifndef UI_cat_H
#define UI_cat_H
/*
** UI_cat.h Catalog Header File
*/

#include<exec/types.h>

struct FC_Type
{
	LONG ID;
	STRPTR Str;
};

extern const struct FC_Type _MSG_SCREENTITLE_SCR;
#define MSG_SCREENTITLE_SCR ((APTR) &_MSG_SCREENTITLE_SCR)
extern const struct FC_Type _MSG_win__WND;
#define MSG_win__WND ((APTR) &_MSG_win__WND)
extern const struct FC_Type _MSG__GAD;
#define MSG__GAD ((APTR) &_MSG__GAD)
extern const struct FC_Type _MSG__GAD;
#define MSG__GAD ((APTR) &_MSG__GAD)
extern const struct FC_Type _MSG__GAD;
#define MSG__GAD ((APTR) &_MSG__GAD)
extern const struct FC_Type _MSG__GAD;
#define MSG__GAD ((APTR) &_MSG__GAD)
extern const struct FC_Type _MSG_AmigaID_lbl_GAD;
#define MSG_AmigaID_lbl_GAD ((APTR) &_MSG_AmigaID_lbl_GAD)
extern const struct FC_Type _MSG__GAD;
#define MSG__GAD ((APTR) &_MSG__GAD)
extern const struct FC_Type _MSG_AmigaMapID_lbl_GAD;
#define MSG_AmigaMapID_lbl_GAD ((APTR) &_MSG_AmigaMapID_lbl_GAD)
extern const struct FC_Type _MSG__GAD;
#define MSG__GAD ((APTR) &_MSG__GAD)
extern const struct FC_Type _MSG_Info_lbl_GAD;
#define MSG_Info_lbl_GAD ((APTR) &_MSG_Info_lbl_GAD)


#endif /* UI_cat_H */
