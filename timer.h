/*
	AmigaMapPing
	Putting you Amiga on the map!



	Copyright 2021 Tygre <tygre@chingu.asia>

	This file is part of AmigaMapPing.

	AmigaMapPing is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmigaMapPing is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmigaMapPing. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/

#ifndef TIMER_H
#define TIMER_H 1

struct timerequest *timer_create(void);
int                 timer_start (struct timerequest *timereq, int minutes);
BOOL                timer_done  (struct timerequest *timereq);
void                timer_free  (struct timerequest *timereq);

#endif
