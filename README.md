Short:        Shows your Amiga on live.amigamap.com
Uploader:     tygre@chingu.asia
Author:       tygre@chingu.asia and mario@amigamap.com
Type:         util/misc
Version:      0.9
Architecture: m68k-amigaos >= 2.0.4
Replaces:     util/misc/AmigaMapPing*

*** AmigaMapPing

AmigaMap (http://amigamap.com) catalogues and shows all existing Amigas in the
world through an easy-to-use interface. It also offers a *live* map of running
Amigas connected to the Internet and AmigaMapPing is the Amiga client that pings
http://live.amigamap.com to tell it that your Amiga is alive and kicking!

*** Essentials

AmigaMapPing is a commodity that includes a small HTTP client to call the REST
APIs offered by live.amigamap.com. It only sends (1) a random, unique ID to
distinguish Amigas that could have the same visible IP address and (2) the
ID  of the Amigas from http://amigamap.com if registered and if provided in the
tool-types of its icon. It pings live.amigamap.com every 15 minutes. It requires
a TCP/IP stack, an Internet connection, and Workbench 2.0.4 or more.

AmigaMapPing is controlled by its tool-types, which work also the same on the
command line. It understands:
- ID=XXX
	Where XXX is the ID of your Amiga computer on amigamap.com. If XXX is valid
	on amigamap.com, then AmigaMapPing displays some of the information that you
	provided on amigamap.com, e.g., your Amiga model, its Kickstart...
- LOCATION=ANONYMOUS|STATIC|GEOIP
	To show your location as the country in which your Amiga lives, OR based on
	the postal code/coordinates given to amigamap.com, OR using the geolocation
	of your public IP address.
- CX_POPUP=YES|NO
	To show or not the window at startup. If you put AmigaMapPing in your
	WBStartup, it is probably best to use CX_POPUP=NO.
- CX_PRIORITY=NNN
	Where NNN is the priority of AmigaMapPing over other commodities. Can be
	safely ignored or set to CX_PRIORITY=0.
- WIN_LEFT=XXX
- WIN_TOP=YYY
	Where XXX and YYY are the x and y coordinates of the top-left corner of the
	Window. The Window positions are also saved (as tool types) automatically
	when stopping AmigaMapPing.
If If you put AmigaMapPing in your WBStartup, do not forget to add DONOTWAIT.

MORE INFORMATION AT http://www.chingu.asia/wiki/index.php?title=AmigaMapPing

*** License

AmigaMapPing is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

AmigaMapPing is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
AmigaMapPing. If not, see <http://www.gnu.org/licenses/>.
