/*
	AmigaMapPing
	Putting you Amiga on the map!



	Copyright 2021 Tygre <tygre@chingu.asia>

	This file is part of AmigaMapPing.

	AmigaMapPing is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmigaMapPing is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmigaMapPing. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/

/* /// "constants" */

#define DEBUG                 FALSE
#define LIBRARYVERSION        37L

#define PROGRAMNAME           "AmigaMapPing"
#define VERSION               1
#define REVISION              0
#define VERSIONSTRING         "1.0"
#define WINLEFTEDGE           10
#define WINTOPEDGE            25

#define TTAMIGAMAPPINGID_NUM  0
#define TTAMIGAMAPPINGID      "ID"
#define TTAMIGAMAPPINGLOC_NUM 1
#define TTAMIGAMAPPINGLOC     "LOCATION"
#define TTCOMMCXPOPUP_NUM     2
#define TTCOMMCXPOPUP         "CX_POPUP"
#define TTCOMMCXPRIORIY_NUM   3
#define TTCOMMCXPRIORIY       "CX_PRIORITY"
#define TTWINDOWLEFTEDGE_NUM  4
#define TTWINDOWLEFTEDGE      "WIN_LEFT"
#define TTWINDOWTOPEDGE_NUM   5
#define TTWINDOWTOPEDGE       "WIN_TOP"
#define RDARGSOPTIONS         6
#define RDARGSTEMPLATE        TTAMIGAMAPPINGID "," TTAMIGAMAPPINGLOC "/K," TTCOMMCXPOPUP "/K," TTCOMMCXPRIORIY "/K," TTWINDOWLEFTEDGE "/K," TTWINDOWTOPEDGE "/K"
#define TTCOMMCXPOPUP_NO      "no"
#define TTAMIGAMAPPINGLOC_V1  "anonymous"
#define TTAMIGAMAPPINGLOC_V2  "static"
#define TTAMIGAMAPPINGLOC_V3  "geoip"
#define FILEAMIGAMAPPINGUID	  "ENVARC:AmigaMapPingUID"

#define USERAGENT             "User-Agent: " PROGRAMNAME " v" VERSIONSTRING
#define URLBASE               "hmtai/t.gmpiao:pmc/aa./p"
#define URLPARTUID            "uid/"
#define URLPARTPING           "ping/"

#define ANSWERPARTUID         "\"uid\":\""
#define ANSWERPARTNAME        "\"name\":\""
#define ANSWERPARTCOUNTRY     "\"country\":\""
#define ANSWERPARTKICKSTRART  "\"kickstart\":\""
#define ANSWERPARTWORKBENCH   "\"os\":\""
#define ANSWERPARTDESCRIPTION "\"description\":\""
#define MAXLENGTH             22

#define DELAYMINUTES          15

/* /// */

/* /// "includes" */

/* typical standard headers */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>

/* typical Amiga headers */

#include <exec/exec.h>
#include <dos/dos.h>
#include <dos/dostags.h>
#include <dos/dosextens.h>
#include <dos/datetime.h>
#include <graphics/gfx.h>
#include <graphics/gfxmacros.h>
#include <graphics/layers.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>
#include <workbench/workbench.h>
#include <workbench/startup.h>
#include <workbench/icon.h>
#include <datatypes/pictureclass.h>
#include <libraries/asl.h>
#include <libraries/commodities.h>
#include <libraries/gadtools.h>
#include <libraries/iffparse.h>
#include <libraries/locale.h>
#include <rexx/rxslib.h>
#include <rexx/storage.h>
#include <rexx/errors.h>
#include <utility/hooks.h>

/* typical proto files */

#include <proto/asl.h>
#include <proto/commodities.h>
#include <proto/datatypes.h>
#include <proto/diskfont.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/gadtools.h>
#include <proto/graphics.h>
#include <proto/icon.h>
#include <proto/iffparse.h>
#include <proto/intuition.h>
#include <proto/layers.h>
#include <proto/locale.h>
#include <proto/rexxsyslib.h>
#include <proto/utility.h>
#include <proto/wb.h>

/* Additional headers */

#include "fortify.h"
#include "http.h"
#include "log.h"
#include "timer.h"
#include "utils.h"

/* /// */

/* /// "prototypes" */

extern int            main   (int argc, char **argv);
static int            clmain (void);
extern int            wbmain (struct WBStartup *wbs);
static struct Config *init   (void);
static void           process(struct Config *);
static void           stop   (struct Config *);

static int            create_broker(const struct Config *, CxObj **);
static void           close_broker (void);

static void           setup_window(const struct Config *);
static int            open_window (void);
static void           close_window(void);
static void           save_window (const BPTR, const char *);

static int            begin_ping_wait(const struct Config *);
static void           loop_ping_wait (const char *, const char *, const char *, const char *);
static char          *get_base_URL   (void);
static char          *get_ID_amigamap(const struct Config *);
static char          *get_ID_amiga   (const char *);
static char          *get_location   (const struct Config *);

static void			  add_gadgets(void);
static void			  add_menus(void);
static void           parse_JSON(const char *, const char *, char **);
static BOOL           continue_long_operation(void);

static void           display_ID         (const char *);
static void           display_Name       (const char *);
static void           display_Country    (const char *);
static void           display_Kickstart  (const char *);
static void           display_Workbench  (const char *);
static void           display_Description(const char *);
static void           display_Status     (const char *);
static void           display_text       (const struct Gadget *, const char *);

/* /// */

/* /// "globals" */

const UBYTE           VersionTag[] = "$VER: " PROGRAMNAME " v" VERSIONSTRING " by Tygre (" __DATE__ ")\n\0";
const struct TextAttr topaz8       = { "topaz.font", 8, 0, 1 };

struct Library       *CxBase                  = NULL;
struct Library       *GadToolsBase            = NULL;
struct Library       *IconBase                = NULL;
struct IntuitionBase *IntuitionBase           = NULL;

static CxObj                *_cxo             = NULL;
static struct NewBroker      _brk             = {
	NB_VERSION,
	PROGRAMNAME,
	PROGRAMNAME " v" VERSIONSTRING,
	"Shows your Amiga on live.amigamap.com",
	NBU_UNIQUE | NBU_NOTIFY,
	COF_SHOW_HIDE,
	0,
	NULL,
	0
};

static int                   _win_left_edge   = 0;
static int                   _win_top_edge    = 0;
static BOOL                  _win_pos_changed = FALSE;
static struct Window        *_win             = NULL;
static struct Screen        *_win_screen      = NULL;
static APTR                  _win_visual_info = 0;
static struct Gadget        *_win_gadget_list = NULL;

/* /// */

/* /// "entry" */

/* ----------------------------------- main ------------------------------------

 Shell/wb entry point

*/

int main(
	int argc,
	char **argv)
{
	int rc = 0;

	if(argc)
	{
		rc = clmain();
	}
	else
	{
		rc = wbmain((struct WBStartup *)argv);
	}

	return rc;
}                                       

/* ---------------------------------- Config -----------------------------------

 Global data for this program

*/

struct Config
{
	struct RDArgs *RDArgs;
	LONG           Options[RDARGSOPTIONS];
	BPTR           ProgramLock;
	STRPTR         ProgramName;
};

/* ---------------------------------- wbmain -----------------------------------

 CLI entry point

*/

static int clmain(
	void)
{
	struct CommandLineInterface *cli    = NULL;
	char                        *name   = NULL;
	struct Config               *config = NULL;
	int                          rc     = 0;

	Fortify_EnterScope();
	log_init_outputs(NULL, NULL, NULL, NULL, NULL);

	config = init();
	if(NULL == config)
	{
		return 20;
	}

	config->RDArgs = ReadArgs(RDARGSTEMPLATE, config->Options, NULL);
	if(NULL == config->RDArgs)
	{
		log_print_error("Error %d, maybe a missing/mispelled keyword among %s?\n", IoErr(), RDARGSTEMPLATE);
		return 5;
	}

	config->ProgramLock = GetProgramDir();
	if(0 == config->ProgramLock)
	{
		log_print_error("Could not lock on program directory\n");
		return 20;
	}
	cli  = BADDR(((struct Process *)FindTask(NULL))->pr_CLI);
	name = BADDR(cli->cli_CommandName);
	config->ProgramName = name;

	process(config);
	rc = begin_ping_wait(config);
	stop(config);

	log_close_outputs();
	Fortify_LeaveScope();
	Fortify_OutputStatistics();

	return rc;
}

/* ---------------------------------- wbmain -----------------------------------

 Workbench entry point

*/

int wbmain(
	struct WBStartup *wbs)
{
	struct Config     *config = NULL;
	int                rc     = 0;
	int                i      = 0;
	struct WBArg      *wbarg  = NULL;
	BPTR               olddir = 0;
	struct DiskObject *dobj   = NULL;
	STRPTR            *ta     = NULL;
	STRPTR            *tb     = NULL;
	UBYTE             *s      = NULL;
	char			  *t      = NULL;

	Fortify_EnterScope();
	log_init_outputs(NULL, NULL, NULL, NULL, NULL);

	config = init();
	if(NULL == config)
	{
		return 20;
	}

	IconBase = OpenLibrary("icon.library", LIBRARYVERSION);
	if(NULL == IconBase)
	{
		log_print_error("Could not open icon.library v%s+\n", LIBRARYVERSION);
		return 20;
	}

	for(i = 0, wbarg = wbs->sm_ArgList;
		i < wbs->sm_NumArgs;
		i++, wbarg++)
	{
		olddir = (BPTR) -1;
		if(wbarg->wa_Lock && *wbarg->wa_Name)
		{
			olddir = CurrentDir(wbarg->wa_Lock);
		}

		if(*wbarg->wa_Name && (dobj = GetDiskObject(wbarg->wa_Name)))
		{
			config->ProgramLock = wbarg->wa_Lock;
			config->ProgramName = wbarg->wa_Name;
			
			ta = dobj->do_ToolTypes;

			s  = FindToolType(ta, TTAMIGAMAPPINGID);
			if(s)
			{
				config->Options[TTAMIGAMAPPINGID_NUM] = (LONG)malloc((strlen(s) + 1) * sizeof(char));
				t    = (char *)config->Options[TTAMIGAMAPPINGID_NUM];
				t[0] = '\0';
				strncat(t, s, strlen(s));
			}
			s  = FindToolType(ta, TTAMIGAMAPPINGLOC);
			if(s)
			{
				config->Options[TTAMIGAMAPPINGLOC_NUM] = (LONG)malloc((strlen(s) + 1) * sizeof(char));
				t    = (char *)config->Options[TTAMIGAMAPPINGLOC_NUM];
				t[0] = '\0';
				strncat(t, s, strlen(s));
			}
			s  = FindToolType(ta, TTCOMMCXPOPUP);
			if(s)
			{
				config->Options[TTCOMMCXPOPUP_NUM] = (LONG)malloc((strlen(s) + 1) * sizeof(char));
				t    = (char *)config->Options[TTCOMMCXPOPUP_NUM];
				t[0] = '\0';
				strncat(t, s, strlen(s));
			}
			s  = FindToolType(ta, TTCOMMCXPRIORIY);
			if(s)
			{
				config->Options[TTCOMMCXPRIORIY_NUM] = (LONG)malloc((strlen(s) + 1) * sizeof(char));
				t    = (char *)config->Options[TTCOMMCXPRIORIY_NUM];
				t[0] = '\0';
				strncat(t, s, strlen(s));
			}
			s  = FindToolType(ta, TTWINDOWLEFTEDGE);
			if(s)
			{
				config->Options[TTWINDOWLEFTEDGE_NUM] = (LONG)malloc((strlen(s) + 1) * sizeof(char));
				t    = (char *)config->Options[TTWINDOWLEFTEDGE_NUM];
				t[0] = '\0';
				strncat(t, s, strlen(s));
			}
			s  = FindToolType(ta, TTWINDOWTOPEDGE);
			if(s)
			{
				config->Options[TTWINDOWTOPEDGE_NUM] = (LONG)malloc((strlen(s) + 1) * sizeof(char));
				t    = (char *)config->Options[TTWINDOWTOPEDGE_NUM];
				t[0] = '\0';
				strncat(t, s, strlen(s));
			}
			
			FreeDiskObject(dobj);
		}

		if((BPTR) -1 != olddir)
		{
			CurrentDir(olddir);
		}
	}

	CloseLibrary(IconBase);

	process(config);
	rc = begin_ping_wait(config);
	stop(config);

	log_close_outputs();
	Fortify_LeaveScope();
	Fortify_OutputStatistics();

	return rc;
}

/* ---------------------------------- Config -----------------------------------

 Functions to handle global data

*/

static struct Config *init(
	void)
{
	struct Config *config = NULL;

	config = (struct Config *)malloc(sizeof(struct Config));
	if(NULL == config)
	{
		Printf("Could not initialise configuration\n");
		return NULL;
	}
	memset(config, 0, sizeof(struct Config));

	return config;
}

static void process(
	struct Config *config)
{
	int   i = 0;
	char *s = NULL;

	if(config->Options)
	{
		for(i = 0; i < RDARGSOPTIONS; i++)
		{
			s = (char *)config->Options[i];
			for( ; s && *s; ++s) *s = tolower(*s);
		}
	}
}

static void stop(
	struct Config *config)
{
	int i = 0;

	if(config)
	{
		if(config->RDArgs)
		{
			FreeArgs(config->RDArgs);
		}
		else if(config->Options)
		{
			for(i = 0; i < RDARGSOPTIONS; i++)
			{
				if(config->Options[i])
				{
					free((void *)config->Options[i]);
				}
			}
		}
		// UnLock(config->ProgramLock);
		free(config);
	}
}

/* /// */

/* /// "main" */

/* ----------------------------------- Main ------------------------------------

 Main program (return codes: 0=ok, 5=warn, 20=fatal)

*/

static int create_broker(
	const struct Config  *config,
	CxObj               **cxob)
{
	struct MsgPort *port = NULL;
	LONG            erro = 0;

	CxBase = OpenLibrary("commodities.library", LIBRARYVERSION);
	if(NULL == CxBase)
	{
		log_print_error("Could not open commodities.library v%s+\n", LIBRARYVERSION);
		return RETURN_ERROR;
	}

	port = CreateMsgPort();
	if(NULL == port)
	{
		log_print_error("Could not create message port\n");
		return RETURN_ERROR;
	}
	_brk.nb_Port = port;


	if(config->Options && config->Options[TTCOMMCXPRIORIY_NUM])
	{
		_brk.nb_Pri = atoi((char *)config->Options[TTCOMMCXPRIORIY_NUM]);
	}

	*cxob = CxBroker(&_brk, &erro);
	if(NULL == *cxob)
	{
		if(CBERR_DUP == erro)
		{
			// No message to show, the existing instance will show itself
			//	log_print_error("Only one instance of AmigaMapPing running makes sense\n");
		}
		else
		{
			log_print_error("Could not create broker (%ld)\n", erro);
		}
		return RETURN_ERROR;
	}
	else
	{
		ActivateCxObj(*cxob, 1L);
		return RETURN_OK;
	}
}

static void close_broker(
	void)
{
	struct MsgPort *prt = NULL;
	struct Message *msg = NULL;

	if(_cxo)
	{
		DeleteCxObj(_cxo);
	}

	Forbid();
	prt = _brk.nb_Port;
	while(msg = GetMsg(prt))
	{
		ReplyMsg(msg);
	}
	DeleteMsgPort(prt);
	Permit();

	if(CxBase)	CloseLibrary(CxBase);
}

static void setup_window(
	const struct Config *config)
{
	if(config->Options && config->Options[TTWINDOWLEFTEDGE_NUM])
	{
		_win_left_edge = atoi((char *)config->Options[TTWINDOWLEFTEDGE_NUM]);
	}
	else
	{
		_win_left_edge = WINLEFTEDGE;
	}
	
	if(config->Options && config->Options[TTWINDOWTOPEDGE_NUM])
	{
		_win_top_edge = atoi((char *)config->Options[TTWINDOWTOPEDGE_NUM]);
	}
	else
	{
		_win_top_edge = WINTOPEDGE;
	}
}

static int open_window(
	void)
{
	if(_win)
	{
		WindowToFront(_win);
		return RETURN_OK;
	}

	GadToolsBase = OpenLibrary("gadtools.library", LIBRARYVERSION);
	if(NULL == GadToolsBase)
	{
		log_print_error("Could not open gadtools.library v%s+\n", LIBRARYVERSION);
		return RETURN_ERROR;
	}
	
	IntuitionBase = (struct IntuitionBase *)OpenLibrary("intuition.library", LIBRARYVERSION);
	if(NULL == IntuitionBase)
	{
		log_print_error("Could not open intuition.library v%s+\n", LIBRARYVERSION);
		return RETURN_ERROR;
	}

	_win_screen = LockPubScreen(NULL);
	if(NULL == _win_screen)
	{
		log_print_error("Could not lock public screen\n");
		return RETURN_ERROR;
	}

	_win = OpenWindowTags(NULL,
		WA_Left,          _win_left_edge,
		WA_Top,           _win_top_edge,
		WA_InnerWidth,    300,
		WA_InnerHeight,   91,
		WA_AutoAdjust,    TRUE,
		WA_IDCMP,         IDCMP_CHANGEWINDOW | IDCMP_CLOSEWINDOW | IDCMP_GADGETUP | IDCMP_MENUPICK | IDCMP_REFRESHWINDOW,
		WA_Flags,         WFLG_DRAGBAR | WFLG_DEPTHGADGET | WFLG_CLOSEGADGET | WFLG_SMART_REFRESH,
		WA_Title,         PROGRAMNAME " v" VERSIONSTRING,
		WA_PubScreen,     _win_screen,
		TAG_DONE );
	if(NULL == _win)
	{
		log_print_error("Could not create the window\n");
		return RETURN_ERROR;
	}

	_win_visual_info = GetVisualInfo(_win_screen, TAG_DONE);
	if(NULL == _win_visual_info)
	{
		log_print_error("Could not get screen visual info\n");
		return RETURN_ERROR;
	}

	add_gadgets();
	add_menus();

	return RETURN_OK;
}

static void save_window(
	const BPTR  proglock,
	const char *progname)
{
	BPTR               olddir = -1;
	struct DiskObject *dobj   = NULL;
	STRPTR            *ta     = NULL;
	STRPTR            *tb     = NULL;
	int                i      = 0;
	int                left_i = -1;
	int                top_i  = -1;

	if(FALSE == _win_pos_changed)
	{
		return;
	}

	IconBase = OpenLibrary("icon.library", LIBRARYVERSION);
	if(NULL == IconBase)
	{
		log_print_error("Could not open icon.library v%s+\n", LIBRARYVERSION);
		return;
	}

	olddir = CurrentDir(proglock);
	if(olddir)
	{
		if(dobj = GetDiskObject((char *)progname))
		{
			ta = dobj->do_ToolTypes;
			i  = 0;
			while(*ta)
			{
				i++;
				ta++;
			}
			
			// 2 for TTWINDOWLEFTEDGE and TTWINDOWTOPEDGE
			// if they do not exist already and 1 for NULL
			// to terminate the tool-type array properly
			tb = (STRPTR *)calloc(sizeof(STRPTR), i + 2 + 1);
			
			ta = dobj->do_ToolTypes;
			i  = 0;
			while(*ta)
			{
				tb[i] = *ta;
				if(string_start_with(tb[i], TTWINDOWLEFTEDGE))
				{
					left_i = i;
				}
				else if(string_start_with(tb[i], TTWINDOWTOPEDGE))
				{
					top_i = i;
				}
				i++;
				ta++;
			}
			if(-1 == left_i)
			{
				left_i = i;
				i++;
			}
			if(-1 == top_i)
			{
				top_i = i;
				i++;
			}
			tb[left_i] = malloc(MAXLENGTH);
			string_snprintf(tb[left_i], MAXLENGTH, "%s=%d", TTWINDOWLEFTEDGE, _win_left_edge);
			tb[top_i]  = malloc(MAXLENGTH);
			string_snprintf(tb[top_i],  MAXLENGTH, "%s=%d", TTWINDOWTOPEDGE,  _win_top_edge);
			                ta = dobj->do_ToolTypes;
			dobj->do_ToolTypes = tb;
			PutDiskObject((char *)progname, dobj);
			free(tb[left_i]);
			free(tb[top_i]);
			free(tb);
			dobj->do_ToolTypes = ta;

			FreeDiskObject(dobj);
		}
		else
		{
			Printf("Could not read icon and save window position\n");
		}

		CurrentDir(olddir);
	}
	else
	{
		Printf("Could not access icon directory and save window position\n");
	}

	CloseLibrary(IconBase);
}

static void close_window(
	void)
{
	struct MsgPort      *prt = NULL;
	struct IntuiMessage *msg = NULL;

	if(_win)
	{
		Forbid();

		prt = _win->UserPort;
		while(msg = GT_GetIMsg(prt))
		{
			GT_ReplyIMsg(msg);
		}
		ModifyIDCMP(_win, 0L);

		RemoveGList(_win, _win_gadget_list, -1);
		ClearMenuStrip(_win);

		CloseWindow(_win);
		_win = NULL;

		Permit();
	}
	if(_win_gadget_list)
	{
		FreeGadgets(_win_gadget_list);
		_win_gadget_list = NULL;
	}
	if(_win_visual_info)
	{
		FreeVisualInfo(_win_visual_info);
		_win_visual_info = NULL;
	}
	if(_win_screen)
	{
		UnlockPubScreen(NULL, _win_screen);
		_win_screen = NULL;
	}
	if(GadToolsBase)
	{
		CloseLibrary(GadToolsBase);
	}
	if(IntuitionBase)
	{
		CloseLibrary((struct Library *)IntuitionBase);
	}
}

static int begin_ping_wait(
	const struct Config *config)
{
	char *url = NULL;
	char *ida = NULL;
	char *uid = NULL;
	char *loc = NULL;

	// Replace default outputs by display_log_information()
	log_close_outputs();
	log_init_outputs(
		display_Status,
		display_Status,
		display_Status,
		display_Status,
		display_Status);

	if(RETURN_ERROR == create_broker(config, &_cxo))
	{
		goto _RETURN_ERROR;
	}

	setup_window(config);
	if(	  0  == config->Options[TTCOMMCXPOPUP_NUM] ||
	   FALSE == string_equal(TTCOMMCXPOPUP_NO, (char *)config->Options[TTCOMMCXPOPUP_NUM]))
	{
		if(RETURN_ERROR == open_window())
		{
			goto _RETURN_ERROR;
		}
	}

	url	= get_base_URL();
	if(NULL == url)
	{
		goto _RETURN_ERROR;
	}

	ida  = get_ID_amigamap(config);
	if(NULL == ida)
	{
		goto _RETURN_ERROR;
	}

	uid  = get_ID_amiga(url);
	if(NULL == uid)
	{
		goto _RETURN_ERROR;
	}

	loc = get_location(config);
	if(NULL == loc)
	{
		goto _RETURN_ERROR;
	}

	loop_ping_wait(url, ida, uid, loc);

	goto _RETURN_OK;
	_RETURN_OK:
		save_window(config->ProgramLock, config->ProgramName);
		close_window();
		close_broker();
		free(url);
		free(uid);
		free(ida);
		return 0;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(config)
		{
			save_window(config->ProgramLock, config->ProgramName);
		}
		close_window();
		close_broker();
		if(url)
		{
			free(url);
		}
		if(uid)
		{
			free(uid);
		}
		if(ida)
		{
			free(ida);
		}
		return 20;
}

static void loop_ping_wait(
	const char          *bas,
	const char          *ida,
	const char          *uid,
	const char          *loc)
{
	char               *idb         = NULL;
	char               *url         = NULL;
	struct timerequest *timereq     = NULL;
	BOOL                ping_on     = FALSE;
	BOOL                wait_on     = FALSE;
	int 			    count       = 0;
	http_response      *hresp       = NULL;
	char               *ans         = NULL;
	char               *name        = NULL;
	char               *country     = NULL;
	char               *kickstart   = NULL;
	char               *workbench   = NULL;
	char               *description = NULL;
	ULONG				commsig     = 0;
	ULONG				timesig     = 0;
	ULONG				windsig     = 0;
	ULONG				signal      = 0;
	struct Message     *msg         = NULL;
	ULONG               msg_class   = 0;
	ULONG               msg_id      = 0;

	idb = malloc(strlen(ida) + 1 + strlen(loc) + 1);
	if(NULL == idb)
	{
		log_print_error("Could not allocate idb\n");
		goto _RETURN_ERROR;
	}
	idb[0] = '\0';
	strcat(idb, ida);
	strcat(idb, " ");
	strcat(idb, loc);
	display_ID(idb);

	url = malloc(strlen(bas) + strlen(URLPARTPING) + strlen(ida) + 1 + strlen(uid) + 1 + strlen(loc) + 1);
	if(NULL == url)
	{
		log_print_error("Could not allocate url\n");
		goto _RETURN_ERROR;
	}
	url[0] = '\0';
	strcat(url, bas);
	strcat(url, URLPARTPING);
	strcat(url, ida);
	strcat(url, "/");
	strcat(url, uid);
	strcat(url, "/");
	strcat(url, loc);

	name        = calloc(1, MAXLENGTH);
	country     = calloc(1, MAXLENGTH);
	kickstart   = calloc(1, MAXLENGTH);
	workbench   = calloc(1, MAXLENGTH);
	description	= calloc(1, MAXLENGTH);
	if(NULL == name      ||
	   NULL == country   ||
	   NULL == kickstart ||
	   NULL == workbench ||
	   NULL == description)
	{
		log_print_error("Could not allocate values\n");
		goto _RETURN_ERROR;
	}

	timereq = timer_create();
	ping_on = TRUE;
	do
	{
		log_print_information("Server update #%d...\n", ++count);
		if(RETURN_OK != http_init_connection(continue_long_operation))
		{
			log_print_error("Could not reach server\n");
			// Will try again after DELAYMINUTES
			//	goto _RETURN_ERROR;
		}
		else if(RETURN_OK != http_get(url, USERAGENT, &hresp))
		{
			log_print_error("Could not get an answer\n");
			// Will try again after DELAYMINUTES
			//	goto _RETURN_ERROR;
		}
		else
		{
			ans = malloc(hresp->body_size_in_bytes + 1);
			ans[0] = '\0';
			strncat(ans, hresp->body, hresp->body_size_in_bytes);

			parse_JSON(ans, ANSWERPARTNAME,        &name);
			parse_JSON(ans, ANSWERPARTCOUNTRY,     &country);
			parse_JSON(ans, ANSWERPARTKICKSTRART,  &kickstart);
			parse_JSON(ans, ANSWERPARTWORKBENCH,   &workbench);
			parse_JSON(ans, ANSWERPARTDESCRIPTION, &description);

			free(ans);
			ans = NULL;
		}
		http_free_http_response(hresp);
		hresp = NULL;
		http_close_connection();

		timesig = timer_start(timereq, DELAYMINUTES);
		wait_on = TRUE;
		do
		{
			display_ID(idb);
			display_Name(name);
			display_Country(country);
			display_Kickstart(kickstart);
			display_Workbench(workbench);
			display_Description(description);

			log_print_information("Sleeping (#%d)...\n", count);
			if(_win)
			{
				commsig = 1L << _brk.nb_Port->mp_SigBit;
				windsig = 1L << _win->UserPort->mp_SigBit;
				signal = Wait(SIGBREAKF_CTRL_C | timesig | commsig | windsig);
			}
			else
			{
				commsig = 1L << _brk.nb_Port->mp_SigBit;
				signal = Wait(SIGBREAKF_CTRL_C | timesig | commsig);
			}

			// Commodity
			if((signal & commsig))
			{
				msg       = GetMsg(_brk.nb_Port);
				msg_class = CxMsgType((CxMsg *)msg);
				msg_id    = CxMsgID((CxMsg *)msg);
				ReplyMsg(msg);
				while(msg = GetMsg(_brk.nb_Port))
				{
					ReplyMsg(msg);
				}

				if(CXM_COMMAND != msg_class)
				{
					continue;
				}
				
				switch(msg_id)
				{
				case CXCMD_APPEAR:
					if(RETURN_ERROR == open_window())
					{
						// Problem (re)opening the window
						// Stop waiting and stop pinging
						wait_on = FALSE;
						ping_on = FALSE;
					}
					break;
				case CXCMD_DISAPPEAR:
					close_window();
					break;
				case CXCMD_KILL:
					// Stop waiting and stop pinging
					wait_on = FALSE;
					ping_on = FALSE;
					break;
				case CXCMD_UNIQUE:
					if(RETURN_ERROR == open_window())
					{
						// Problem (re)opening the window
						// Stop waiting and stop pinging
						wait_on = FALSE;
						ping_on = FALSE;
					}
					break;
				}
			}
			// Timer
			else if((signal & timesig) && timer_done(timereq))
			{
				// Stop waiting but continue pinging
				wait_on = FALSE;
				ping_on = TRUE;
			}
			// User
			else if((signal & SIGBREAKF_CTRL_C))
			{
				// Stop waiting and stop pinging
				wait_on = FALSE;
				ping_on = FALSE;
			}
			// Window
			else if((signal & windsig))
			{
				msg       = GetMsg(_win->UserPort);
				msg_class = ((struct IntuiMessage *)msg)->Class;
				msg_id    = ((struct IntuiMessage *)msg)->Code;
				ReplyMsg(msg);
				while(msg = GetMsg(_win->UserPort))
				{
					ReplyMsg(msg);
				}
				
				if(IDCMP_CHANGEWINDOW == msg_class)
				{
					_win_left_edge   = _win->LeftEdge;
					_win_top_edge    = _win->TopEdge;
					_win_pos_changed = TRUE;
				}
				else if(IDCMP_CLOSEWINDOW == msg_class)
				{
					close_window();
				}
				else if(IDCMP_MENUPICK == msg_class)
				{
					msg_id = ITEMNUM(msg_id);
					switch(msg_id)
					{
					case 0:
						display_Status("By tygre@chingu.asia\n");
						Delay(150);
						break;
					case 2:
						// Stop waiting but continue pinging
						wait_on = FALSE;
						ping_on = TRUE;
						break;
					case 3:
						close_window();
						break;
					case 5:
						// Stop waiting and stop pinging
						wait_on = FALSE;
						ping_on = FALSE;
						break;
					}
				}
				else if(IDCMP_REFRESHWINDOW == msg_class)
				{
					GT_BeginRefresh(_win);
					GT_EndRefresh(_win, TRUE);
				}
			}
		}
		while(wait_on);
	}
	while(ping_on);

	goto _RETURN_OK;
	_RETURN_OK:
		free(idb);
		free(url);
		free(name);
		free(country);
		free(kickstart);
		free(workbench);
		free(description);
		timer_free(timereq);
		return;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(idb)
		{
			free(idb);
		}
		if(url)
		{
			free(url);
		}
		if(name)
		{
			free(name);
		}
		if(country)
		{
			free(country);
		}
		if(kickstart)
		{
			free(kickstart);
		}
		if(workbench)
		{
			free(workbench);
		}
		if(description)
		{
			free(description);
		}
		timer_free(timereq);
		http_free_http_response(hresp);
		if(ans)
		{
			free(ans);
		}
		return;
}

static char *get_base_URL(
	void)
{
	char *base = NULL;

	base = malloc(strlen(URLBASE) + 1);
	if(NULL == base)
	{
		log_print_error("Could not allocate base URL\n");
		return NULL;
	}
	string_decipher(7, URLBASE, base);

	return base;
}

static char *get_ID_amigamap(
	const struct Config *config)
{
	char *ida = NULL;

	if(config->Options && config->Options[TTAMIGAMAPPINGID_NUM])
	{
		ida = string_duplicate((char *)config->Options[TTAMIGAMAPPINGID_NUM]);
	}
	else
	{
		log_print_information("No AmigaMap ID provided\n");
		ida = malloc(2);
		if(NULL == ida)
		{
			log_print_error("Could not allocate AmigaMap ID\n");
			return NULL;
		}
		ida[0] = '\0';
		strncat(ida, "0", 1);
	}

	return ida;
}

static char *get_ID_amiga(
	const char *base)
{
	char          *url   = NULL;
	char          *ans   = NULL;
	char          *uid   = NULL;
	BPTR           fh    = 0;
	http_response *hresp = NULL;

	// Does this Amiga has its own UID?
	// If not, I get and store one.
	if(0 == Lock(FILEAMIGAMAPPINGUID, ACCESS_READ))
	{
		// No UID
		log_print_warning("No previous UID\n");

		url    = malloc(strlen(base) + strlen(URLPARTUID) + 1);
		url[0] = '\0';
		strcat(url, base);
		strcat(url, URLPARTUID);
		log_print_information("Contacting server...\n");

		// GET JSON
		if(RETURN_OK != http_init_connection(continue_long_operation))
		{
			log_print_error("Could not init connection\n");
			goto _RETURN_ERROR;
		}
		if(RETURN_OK != http_get(url, USERAGENT, &hresp))
		{
			log_print_error("Could not get an answer\n");
			goto _RETURN_ERROR;
		}
		
		ans = malloc(hresp->body_size_in_bytes + 1);
		ans[0] = '\0';
		strncat(ans, hresp->body, hresp->body_size_in_bytes);
		
		http_free_http_response(hresp);
		hresp = NULL;
		http_close_connection();
		free(url);
		url = NULL;

		// Find UID in JSON
		uid = calloc(1, MAXLENGTH);
		if(NULL == uid)
		{
			goto _RETURN_ERROR;
		}
		parse_JSON(ans, ANSWERPARTUID, &uid);
		if(NULL == uid)
		{
			goto _RETURN_ERROR;
		}
		free(ans);
		ans = NULL;

		// Save UID
		fh = Open(FILEAMIGAMAPPINGUID, MODE_NEWFILE);
		if(0 == fh)
		{
			log_print_error("Could not open file\n");
			goto _RETURN_ERROR;
		}
		if(-1 == FPuts(fh, uid))
		{
			log_print_error("Could not write UID\n");
			goto _RETURN_ERROR;
		}
		Close(fh);
		fh = 0;
	}
	else
	{
		// UID exists
		fh = Open(FILEAMIGAMAPPINGUID, MODE_OLDFILE);
		if(0 == fh)
		{
			log_print_error("Could not open file\n");
			goto _RETURN_ERROR;
		}
		uid = malloc(MAXLENGTH);
		if(NULL == uid)
		{
			log_print_error("Could not allocate UID\n");
			goto _RETURN_ERROR;
		}
		if(NULL == FGets(fh, uid, MAXLENGTH))
		{
			log_print_error("Could not read UID\n");
			goto _RETURN_ERROR;
		}
		Close(fh);
		fh = 0;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		return uid;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(url)
		{
			free(url);
		}
		if(ans)
		{
			free(ans);
		}
		if(uid)
		{
			free(uid);
		}
		if(fh)
		{
			Close(fh);
		}
		http_free_http_response(hresp);
		http_close_connection();
		return NULL;
}

static char *get_location(
	const struct Config *config)
{
	if(config->Options && config->Options[TTAMIGAMAPPINGLOC_NUM])
	{
		if(string_equal(TTAMIGAMAPPINGLOC_V1, (char *)config->Options[TTAMIGAMAPPINGLOC_NUM]) ||
		   string_equal(TTAMIGAMAPPINGLOC_V2, (char *)config->Options[TTAMIGAMAPPINGLOC_NUM]) ||
		   string_equal(TTAMIGAMAPPINGLOC_V3, (char *)config->Options[TTAMIGAMAPPINGLOC_NUM]))
		{
			return (char *)config->Options[TTAMIGAMAPPINGLOC_NUM];
		}
	}

	return "";
}

/* /// */

/* /// "helpers" */

static void add_gadgets(
	void)
{
	int               i	                          = 0;
	struct Gadget    *previous_gadget             = NULL;
	#define           nb_gadgets                  7
	struct Gadget    *gadgets[nb_gadgets]         = { NULL };
	struct NewGadget  gadget_datas[nb_gadgets]    = {
		{ 106,  2, 192, 11,  "         ID:", (struct TextAttr *)&topaz8, 0, PLACETEXT_LEFT, NULL, NULL },
		{ 106, 14, 192, 11,  "       Name:", (struct TextAttr *)&topaz8, 0, PLACETEXT_LEFT, NULL, NULL },
		{ 106, 26, 192, 11,  "    Country:", (struct TextAttr *)&topaz8, 0, PLACETEXT_LEFT, NULL, NULL },
		{ 106, 38, 192, 11,  "  Kickstart:", (struct TextAttr *)&topaz8, 0, PLACETEXT_LEFT, NULL, NULL },
		{ 106, 50, 192, 11,  "  Workbench:", (struct TextAttr *)&topaz8, 0, PLACETEXT_LEFT, NULL, NULL },
		{ 106, 62, 192, 11,  "Description:", (struct TextAttr *)&topaz8, 0, PLACETEXT_LEFT, NULL, NULL },
		{ 106, 78, 192, 11,  "     Status:", (struct TextAttr *)&topaz8, 0, PLACETEXT_LEFT, NULL, NULL }
	};
	ULONG             gadget_kinds[nb_gadgets]    = {
		TEXT_KIND,
		TEXT_KIND,
		TEXT_KIND,
		TEXT_KIND,
		TEXT_KIND,
		TEXT_KIND,
		TEXT_KIND
	};
	ULONG             gadget_tags [nb_gadgets][3] = {
		{ GTTX_Border, TRUE, TAG_DONE },
		{ GTTX_Border, TRUE, TAG_DONE },
		{ GTTX_Border, TRUE, TAG_DONE },
		{ GTTX_Border, TRUE, TAG_DONE },
		{ GTTX_Border, TRUE, TAG_DONE },
		{ GTTX_Border, TRUE, TAG_DONE },
		{ GTTX_Border, TRUE, TAG_DONE }
	};

	previous_gadget = CreateContext(&_win_gadget_list);
	if(NULL == previous_gadget)
	{
		log_print_error("Could not create GadTools context\n");
		return;
	}

	for(i = 0; i < nb_gadgets; i++)
	{
		gadget_datas[i].ng_GadgetID    = i;
		gadget_datas[i].ng_VisualInfo  = _win_visual_info;
		gadget_datas[i].ng_LeftEdge   += _win->BorderLeft;
		gadget_datas[i].ng_TopEdge    += _win->BorderTop;

		gadgets[i] = previous_gadget = CreateGadgetA(
			gadget_kinds[i],
			previous_gadget,
			&gadget_datas[i],
			(struct TagItem *)&gadget_tags[i]);
		if(NULL == gadgets[i])
		{
			log_print_error("Could not create gadgets\n");
			return;
		}
	}
	AddGList(_win, _win_gadget_list, 0, -1, NULL);
	RefreshGList(_win_gadget_list, _win, NULL, -1);
}

static void add_menus(
	void)
{
	struct NewMenu menu_items[] = {
		{ NM_TITLE, "AmigaMapPing",   0, 0, 0, 0 },
		{ NM_ITEM,  "About...",     "A", 0, 0, 0 },
		{ NM_ITEM,  NM_BARLABEL,      0, 0, 0, 0 },
		{ NM_ITEM,  "Ping",         "P", 0, 0, 0 },
		{ NM_ITEM,  "Hide",         "H", 0, 0, 0 },
		{ NM_ITEM,  NM_BARLABEL,      0, 0, 0, 0 },
		{ NM_ITEM,  "Quit",         "Q", 0, 0, 0 }
	};
	struct Menu *menu = NULL;

	menu = CreateMenus(menu_items, TAG_END);
	if(NULL == menu)
	{
		log_print_error("Could not create menu\n");
		return;
	}
	if(!LayoutMenus(menu, _win_visual_info, TAG_END))
	{
		log_print_error("Could not lay out menu\n");
		return;
	}
	if(!SetMenuStrip(_win, menu))
	{
		log_print_error("Could not set menu\n");
		return;
	}
}

static void parse_JSON(
	const char  *json,
	const char  *tag,
	char       **out)
{
	char *tmp1  = NULL;
	char *tmp2  = NULL;
	char  c     = '\0';

	// Beginning
	tmp1 = strstr(json, tag);
	if(NULL == tmp1)
	{
		log_print_error("No value tag in the JSON\n");
		return;
	}
	tmp1 = tmp1 + strlen(tag);

	// End
	tmp2 = strchr(tmp1, '\"');
	if(NULL == tmp2)
	{
		log_print_error("No value end in the JSN\n");
		return;
	}
	c = tmp2[0];
	tmp2[0] = '\0';

	// Value
	*out[0] = '\0';
	strncat(*out, tmp1, MAXLENGTH - 1);
	tmp2[0] = c;
}

static BOOL continue_long_operation(void)
{
	// Never interrupt a long-running operation
	return TRUE;
}

static void display_ID(
	const char *msg)
{
	if(_win_gadget_list)
	{
		display_text(_win_gadget_list->NextGadget, msg);
	}
}

static void display_Name(
	const char *msg)
{
	if(_win_gadget_list)
	{
		display_text(_win_gadget_list->NextGadget->NextGadget, msg);
	}
}

static void display_Country(
	const char *msg)
{
	if(_win_gadget_list)
	{
		display_text(_win_gadget_list->NextGadget->NextGadget->NextGadget, msg);
	}
}

static void display_Kickstart(
	const char *msg)
{
	if(_win_gadget_list)
	{
		display_text(_win_gadget_list->NextGadget->NextGadget->NextGadget->NextGadget, msg);
	}
}

static void display_Workbench(
	const char *msg)
{
	if(_win_gadget_list)
	{
		display_text(_win_gadget_list->NextGadget->NextGadget->NextGadget->NextGadget->NextGadget, msg);
	}
}

static void display_Description(
	const char *msg)
{
	if(_win_gadget_list)
	{
		display_text(_win_gadget_list->NextGadget->NextGadget->NextGadget->NextGadget->NextGadget->NextGadget, msg);
	}
}

static void display_Status(
	const char *msg)
{
	// Remove trailing newline
	((char *)msg)[strlen(msg) - 1] = '\0';
	if(_win_gadget_list)
	{
		display_text(_win_gadget_list->NextGadget->NextGadget->NextGadget->NextGadget->NextGadget->NextGadget->NextGadget, msg);
	}
}

static void display_text(
	const struct Gadget *gdt,
	const char          *msg)
{
	if(gdt && _win)
	{
		GT_SetGadgetAttrs((struct Gadget *)gdt, _win, NULL, GTTX_Text, msg, TAG_DONE);
		GT_RefreshWindow(_win, NULL);
	}
	else if(DEBUG)
	{
		Printf(msg);
		Printf("\n");
	}
}

/* /// */

