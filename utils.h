/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2021 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef UTILS_H
#define UTILS_H 1

#include <dos/dos.h>       // For BPTR
#include <dos/dosextens.h> // For struct Process
#include <exec/types.h>    // For ULONG

#define IN    const
#define OUT
#define INOUT

void  chrcat(
	  INOUT char  *string,
	  IN    char   c);

int   integer_max(
	  IN    int    a,
	  IN    int    b);

int   integer_min(
	  IN    int    a,
	  IN    int    b);

void  io_read_line(
	  IN    char(*a_get_char_function)(void),
	  OUT   char **line);

int   io_copy_file(
	  IN    char  *source_file_path,
	  IN    char  *destination_name);

int   io_file_exist(
	  IN    char  *file_name);

BPTR  io_path_list_copy(
	  IN    BPTR   path_list);

void  io_path_list_free(
	  IN    BPTR   path_list_copy);

int   string_count_char(
	  IN    char  *haystack,
	  IN    char   needle);

int   string_end_with(
	  IN    char  *haystack,
	  IN    char  *needle);

int   string_fit(
	  OUT   char **destination,
	  IN    char  *source);

int   string_remove_first_char(
	  OUT   char **bale,
	  IN    char  *haystack,
	  IN    char   needle);

int   string_remove_all_chars(
	  OUT   char **bale,
	  IN    char  *haystack,
	  IN    char   needle);

int   string_start_with(
	  IN    char  *haystack,
	  IN    char  *needle);

char *string_duplicate(
	  IN    char  *s);

char *string_duplicate_n(
	  IN    char  *s,
	  IN    int    max);

BOOL  string_equal(
	  IN    char  *s1,
	  IN    char  *s2);

char *string_replace_all(
	  IN    char  *haystack,
	  IN    char  *old_chars,
	  IN    char  *new_chars);

void  string_encipher(
	  IN    int    key,
	  IN    char  *plain_text,
	  OUT   char  *cipher_text);

void  string_decipher(
	  IN    int    key,
	  IN    char  *cipher_text,
	  OUT   char  *plain_text);

int   string_snprintf(
	  INOUT char  *d,
	  IN    int    n,
	  IN    char  *format,
	  ...);

#endif
