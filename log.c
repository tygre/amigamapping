/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2021 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "locale.h"
#include "log.h"

#include "exec/types.h"
#include "fortify.h"
#include "utils.h"

#include <stdarg.h>     // For va_start() and va_end()
#include <stdio.h>
#include <stdlib.h>
#include <dos/dos.h>    // For RETURN_OK, RETURN_ERROR
#include <proto/dos.h>  // For Printf(), Flush()
#include <proto/exec.h> // For Forbid(), Permit()



/* Constants and declarations */

#define DEBUG          0
#define DEBUG_VERBOSE  0
#define MAXIMUM_LENGTH 1024

	   int  log_init_outputs(
			IN log_print_callback_fp,
			IN log_print_callback_fp,
			IN log_print_callback_fp,
			IN log_print_callback_fp,
			IN log_print_callback_fp);
	   void log_print_debug(IN BOOL, IN char *, ...);
	   void log_print_information(IN char *, ...);
	   void log_print_warning(IN char *, ...);
	   void log_print_error(IN char *, ...);
	   void log_print_fatal_error(IN char *, ...);
	   void log_close_outputs(void);
static void _log_print_problem(IN char *, IN log_print_callback_fp, IN char *, IN va_list);



/* Definitions */

static log_print_callback_fp  _debug_callback       = NULL;
static log_print_callback_fp  _information_callback = NULL;
static log_print_callback_fp  _warning_callback     = NULL;
static log_print_callback_fp  _error_callback       = NULL;
static log_print_callback_fp  _fatal_error_callback = NULL;
static int                    _number_of_inits      = 0;
static char                  *_formatted_message    = NULL;



int log_init_outputs(
	IN log_print_callback_fp debug_callback,
	IN log_print_callback_fp information_callback,
	IN log_print_callback_fp warning_callback,
	IN log_print_callback_fp error_callback,
	IN log_print_callback_fp fatal_error_callback)
{
	_debug_callback       = debug_callback;
	_warning_callback     = warning_callback;
	_information_callback = information_callback;
	_error_callback       = error_callback;
	_fatal_error_callback = fatal_error_callback;
	// Tygre 2017/04/15: Nested init/close
	// I count the number of inits to release
	// resources only on the "last" close...
	_number_of_inits++;

	// Forbid();
	// Tygre 2017/04/01: Reentry
	// Between main() and controls, this function may be called several times in a row.
	if(_formatted_message != NULL)
	{
		if(DEBUG)
		{
			Printf( GetString( MSG_LOG_ERRORINLOGINITOUTPUTSLOGALREADYINITIALISED ) );
		}
		return RETURN_WARN;
	}

	_formatted_message = calloc(MAXIMUM_LENGTH,  sizeof(char));
	if(_formatted_message == NULL)
	{
		Printf( GetString( MSG_LOG_ERRORINLOGINITOUTPUTSCOULDNOTALLOCATEMEMORYFORFORMATTED ) );
		return RETURN_ERROR;
	}
	// Permit();

	return RETURN_OK;
}

void log_print_debug(
	 IN BOOL  should_print,
	 IN char *message,
	 ...)
{
	va_list arguments = NULL;
	if(should_print)
	{
		va_start(arguments, message);
		_log_print_problem("DEBUG", _debug_callback, message, arguments);
		va_end(arguments);
	}
}

void log_print_information(
	 IN char *message,
	 ...)
{
	va_list arguments = NULL;
	va_start(arguments, message);
	_log_print_problem("INFOR", _information_callback, message, arguments);
	va_end(arguments);
}

void log_print_warning(
	 IN char *message,
	 ...)
{
	va_list arguments = NULL;
	va_start(arguments, message);
	_log_print_problem("WARNG", _warning_callback, message, arguments);
	va_end(arguments);
}

void log_print_error(
	 IN char *message,
	 ...)
{
	va_list arguments = NULL;
	va_start(arguments, message);
	_log_print_problem("ERROR", _error_callback, message, arguments);
	va_end(arguments);
}

void log_print_fatal_error(
	 IN char *message,
	 ...)
{
	va_list arguments = NULL;
	va_start(arguments, message);
	_log_print_problem("FATAL", _fatal_error_callback, message, arguments);
	va_end(arguments);
}

static void _log_print_problem(
	 IN char                  *header,
	 IN log_print_callback_fp  callback_fp,
	 IN char                  *message,
	 IN va_list                arguments)
{
	if(_formatted_message == NULL)
	{
		return;
	}

	Forbid();
	vsnprintf(_formatted_message, MAXIMUM_LENGTH - 1, message, arguments);

	// To ensure a properly terminated string in any case
	_formatted_message[MAXIMUM_LENGTH - 1] = '\0';

	if(DEBUG)
	{
		Printf("%s", _formatted_message);
		Flush(Output());
	}
	if(callback_fp != NULL)
	{
		callback_fp(_formatted_message);
	}
	else
	{
		Printf("%s", _formatted_message);
	}
	Permit();
}

void log_close_outputs(void)
{
	_debug_callback       = NULL;
	_information_callback = NULL;
	_warning_callback     = NULL;
	_error_callback       = NULL;
	_fatal_error_callback = NULL;

	// Forbid();
	_number_of_inits--;

	if(_number_of_inits == 0)
	{
		free(_formatted_message);
		_formatted_message = NULL;
	}
	// Permit();
}

