/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2021 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef HTTP_H
#define HTTP_H 1

#include "utils.h"

// A parsed URL
typedef struct parsed_url
{
	const char *uri;      // mandatory
	const char *scheme;   // mandatory
	const char *host;     // mandatory
	const char *ip;       // mandatory
	const char *port;     // optional
	const char *path;     // optional
	const char *query;    // optional
	const char *fragment; // optional
	const char *username; // optional
	const char *password; // optional
} parsed_url;

// A HTTP response
typedef struct http_response
{
	const parsed_url    *request_uri;
	const unsigned char *body;
	const int            body_size_in_bytes;
	const char          *status_code;
	const int            status_code_value;
	const char          *status_text;
	const char          *request_headers;
	const char          *response_headers;
} http_response;

int  http_init_connection(
	 IN  BOOL            (*continue_long_operation)());

int  http_get(
	 IN  char           *url,
	 IN  char           *custom_headers,
	 OUT http_response **hresp);

int  http_head(
	 IN  char           *url,
	 IN  char           *custom_headers,
	 OUT http_response **hresp);

int  http_post(
	 IN  char           *url,
	 IN  char           *custom_headers,
	 IN  char           *post_data,
	 OUT http_response **hresp);

void http_free_http_response(
	 IN  http_response *response);

void http_close_connection(
	 void);

#endif

