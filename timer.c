/*
	AmigaMapPing
	Putting you Amiga on the map!



	Copyright 2021 Tygre <tygre@chingu.asia>

	This file is part of AmigaMapPing.

	AmigaMapPing is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmigaMapPing is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmigaMapPing. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include <devices/timer.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <clib/alib_protos.h> // CreatePort, CreateExtIO...

#include <stdio.h>


/* Constants and declarations */

struct timerequest *timer_create(void);
int                 timer_start (struct timerequest *, int);
BOOL				timer_done  (struct timerequest *);
void                timer_free  (struct timerequest *);

struct Device  *TimerBase = NULL;



/* Definitions */

struct timerequest *timer_create(void)
{
	struct MsgPort     *timermp = NULL;
	struct timerequest *timereq = NULL;

	timermp = CreateMsgPort();
	if(NULL == timermp)
	{
		Printf("Could not create time port\n");
		return NULL;
	}

	timereq = (struct timerequest *)CreateExtIO(timermp, sizeof(struct timerequest));
	if(NULL == timereq)
	{
		Printf("Could not create time request\n");
		DeletePort(timermp);
		return NULL;
	}
	timereq->tr_node.io_Message.mn_Node.ln_Type = 0; // Avoid CheckIO() hanging bug
	timereq->tr_node.io_Flags   = 1;				 // To know if it was sent (will be 0);

	if(0 != OpenDevice("timer.device", UNIT_VBLANK, (struct IORequest *)timereq, 0))
	{
		Printf("Could not open timer.device\n");
		DeleteExtIO((struct IORequest *)timereq);
		DeletePort(timermp);
		return NULL;
	}
	TimerBase = timereq->tr_node.io_Device;

	return timereq;
}

int timer_start(
	struct timerequest *timereq,
	int minutes)
{
	struct MsgPort *timermp = NULL;
	int             timesig = -1;

	if(NULL == timereq)
	{
		return -1;
	}
	else if(0 == timereq->tr_node.io_Flags)
	{
		// Cancel any pending time request
		AbortIO((struct IORequest *)timereq);
		WaitIO ((struct IORequest *)timereq);
	}

	timereq->tr_node.io_Command = TR_ADDREQUEST;
	timereq->tr_node.io_Flags   = 1;
	timereq->tr_time.tv_secs    = minutes * 60;
	timereq->tr_time.tv_micro   = 0;
	SendIO((struct IORequest *)timereq);

	timermp = timereq->tr_node.io_Message.mn_ReplyPort;
	timesig = 1 << timermp->mp_SigBit;
	return timesig;
}

BOOL timer_done(
	struct timerequest *timereq)
{
	struct MsgPort *timermp = NULL;
	int             timesig = -1;

	if(NULL == timereq)
	{
		return FALSE;
	}

	if(CheckIO((struct IORequest *)timereq))
	{
		timermp = timereq->tr_node.io_Message.mn_ReplyPort;
		GetMsg(timermp);

		timesig = 1 << timermp->mp_SigBit;
		SetSignal(0L, timesig);

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

void timer_free(
	struct timerequest *timereq)
{
	struct MsgPort *timermp = NULL;
	
	if(NULL == timereq)
	{
		return;
	}

	timermp = timereq->tr_node.io_Message.mn_ReplyPort;

	// CheckIO() returns NULL if IORequest
	// is in progress... or was never sent?!
	//	if(!CheckIO((struct IORequest *)timereq))
	if(0 == timereq->tr_node.io_Flags)
	{
		AbortIO((struct IORequest *)timereq);
		WaitIO ((struct IORequest *)timereq);
	}
	if(NULL != TimerBase)
	{
		CloseDevice((struct IORequest *)timereq);
	}
	if(NULL != timereq)
	{
		DeleteExtIO((struct IORequest *)timereq);
	}
	if(NULL != timermp)
	{
		DeletePort(timermp);
	}
}
